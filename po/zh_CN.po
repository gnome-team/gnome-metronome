# Chinese (China) translation for metronome.
# Copyright (C) 2022 metronome's COPYRIGHT HOLDER
# This file is distributed under the same license as the metronome package.
# Luming Zh <lumingzh@qq.com>, 2022.
# lumingzh <lumingzh@qq.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: metronome master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/metronome/-/issues\n"
"POT-Creation-Date: 2022-01-23 12:16+0000\n"
"PO-Revision-Date: 2022-01-31 16:37+0800\n"
"Last-Translator: lumingzh <lumingzh@qq.com>\n"
"Language-Team: Chinese - China <i18n-zh@googlegroups.com>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"
"X-Generator: Gtranslator 41.0\n"

#: data/resources/ui/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "常规"

#: data/resources/ui/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "显示快捷键"

#: data/resources/ui/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Quit"
msgstr "退出"

#: data/resources/ui/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Tap"
msgstr "拍子"

#: data/resources/ui/window.ui:5
msgid "_Keyboard Shortcuts"
msgstr "键盘快捷键(_K)"

#: data/resources/ui/window.ui:9
msgid "_About Metronome"
msgstr "关于节拍器(_A)"

#: data/resources/ui/window.ui:58
msgid ""
"2\n"
"4"
msgstr ""
"2\n"
"4"

#: data/resources/ui/window.ui:77
msgid ""
"3\n"
"4"
msgstr ""
"3\n"
"4"

#: data/resources/ui/window.ui:96
msgid ""
"4\n"
"4"
msgstr ""
"4\n"
"4"

#: data/resources/ui/window.ui:115
msgid ""
"6\n"
"8"
msgstr ""
"6\n"
"8"

#: data/resources/ui/window.ui:207
msgid "TAP"
msgstr "拍子"

#: data/com.adrienplazas.Metronome.desktop.in.in:3
#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:7 src/main.rs:25
msgid "Metronome"
msgstr "节拍器"

#: data/com.adrienplazas.Metronome.desktop.in.in:4
#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:8
msgid "Keep the tempo"
msgstr "保持该节奏"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/com.adrienplazas.Metronome.desktop.in.in:10
msgid "Bar;Beat;Beats;BPM;Measure;Minute;Rhythm;Tap;Tempo;"
msgstr ""
"Bar;Beat;Beats;BPM;Measure;Minute;Rhythm;Tap;Tempo;小节;测量;时刻;拍子;节奏;"
"跳动;心跳;"

#: data/com.adrienplazas.Metronome.gschema.xml.in:6
#: data/com.adrienplazas.Metronome.gschema.xml.in:7
msgid "Default window width"
msgstr "默认窗口宽度"

#: data/com.adrienplazas.Metronome.gschema.xml.in:11
#: data/com.adrienplazas.Metronome.gschema.xml.in:12
msgid "Default window height"
msgstr "默认窗口高度"

#: data/com.adrienplazas.Metronome.gschema.xml.in:16
msgid "Default window maximized behaviour"
msgstr "默认窗口最大化行为"

#: data/com.adrienplazas.Metronome.gschema.xml.in:22
#: data/com.adrienplazas.Metronome.gschema.xml.in:23
msgid "Default beats per bar"
msgstr "默认每小节拍子"

#: data/com.adrienplazas.Metronome.gschema.xml.in:28
#: data/com.adrienplazas.Metronome.gschema.xml.in:29
msgid "Default beats per minute"
msgstr "默认每分钟拍子"

#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:10
msgid ""
"Metronome beats the rhythm for you, you simply need to tell it the required "
"time signature and beats per minutes."
msgstr "节拍器可以为您奏出节拍，您只需要告诉它需要的拍子记号和每分钟节拍。"

#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:11
msgid ""
"You can also tap to let the application guess the required beats per minute."
msgstr "您也可以轻击让应用程序猜测每分钟需要的节拍。"

#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:16
msgid "Main window"
msgstr "主窗口"

#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:20
msgid "Main window, narrow and running"
msgstr "狭窄且正在运行的主窗口"

#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:63
msgid "Adrien Plazas"
msgstr "Adrien Plazas"
